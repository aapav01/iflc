---
id: home
blueprint: pages
title: Home
custom_page_header: false
blocks:
  -
    id: lmqaamc1
    version: slider_2
    type: slider
    enabled: true
    slides:
      -
        id: lmqagdxr
        title: 'International Financial Literacy Conclave 2023'
        description: "India's Leading Financial Inclusion"
        type: new_set
        enabled: true
        image: sliders/_mg_5421.png
        overlay_text: '#IFLC2023'
      -
        id: lmrcvm5l
        title: 'We are coming with the 6th Edition of IFLC.'
        type: new_set
        enabled: true
        description: "India's Only Financial Literacy Conference. Join us this 24th November, 2023."
        image: sliders/4r9a2766.png
        overlay_text: '#IFLC2023'
  -
    id: lmrf9btz
    date_field: '2023-11-24'
    type: countdown
    enabled: true
    version: counter_1
    title: 'Join Us on International Financial Literacy Conclave 2023'
  -
    id: lmrlhfwo
    version: contact_4
    enable_location: false
    enable_form: false
    phone_number: '+919219812079'
    type: contact_section
    enabled: true
    email_address: shweta@awokeindia.com
    addresss: India
    address: 'India International Centre, Delhi'
  -
    id: lmrog4vj
    version: stats_section_1
    title: 'Overview of 2022'
    stats:
      -
        id: lmroh1zz
        number: '6'
        description: Session
        has_icon: false
        type: new_set
        enabled: true
      -
        id: lmrohg8x
        number: 30+
        description: Speakers
        has_icon: false
        type: new_set
        enabled: true
      -
        id: lmroi4ay
        number: 15+
        description: Exhibitors
        has_icon: false
        type: new_set
        enabled: true
      -
        id: lmrojf4y
        number: 150+
        description: Delegates
        has_icon: false
        type: new_set
        enabled: true
    type: stats
    enabled: true
  -
    id: lmrmnyo9
    version: team_section_1
    title: 'Dignitaries at #IFLC2022'
    description: 'The objective for the #IFLC22 pivots around this key issue of bringing digitalisation to serve the purpose of skilling & also safeguarding the citizens on financial matters at a speed equal to/greater than a storm of digital financial products & services.'
    members:
      -
        id: lmrmvekq
        full_name: 'Shri Brajesh Pathak'
        position: 'Deputy Chief Minister'
        biography: 'Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
        image: 'rs=w-500,h-500,cg-true.webp'
      -
        id: lmrngmqv
        full_name: 'Shri Manoj Kumar Singh'
        position: 'IAS, Agriculture Production'
        biography: 'Commissioner, Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
        image: 'rs=w-400,h-400,cg-true.webp'
      -
        id: lmrnju3q
        image: 'rs=w-600,h-600,cg-true.webp'
        full_name: 'Shri Navneet Sehgal'
        position: 'IAS, Additional Chief Secretary'
        biography: 'Sports & Youth Welfare, Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnlwq8
        image: 'rs=w-730,h-730,cg-true.webp'
        full_name: 'Shri Ashwani Bhatia'
        position: 'Whole Time Member'
        biography: 'Securities and Exchange Board of India, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnndbl
        image: 'rs=w-730,h-730,cg-true-2.webp'
        full_name: 'G. P. Garg'
        position: 'Executive Director'
        biography: 'Securities and Exchange Board of India, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnq6x8
        image: 'rs=w-730,h-730,cg-true-3.webp'
        full_name: 'Prasun Kumar Das'
        position: 'Secretary General - Asia Pacific'
        biography: 'Rural and Agricultural Credit, Association, Bangkok, Thailand'
        toggle_social: false
        type: set
        enabled: true
    type: team_section
    enabled: true
    link_field: 'entry::dbe134cc-f9d1-49aa-ae57-4f2f43880a29'
  -
    id: lmrpfqos
    version: logo_cloud_7
    images:
      - 'rs=w-200,h-200,cg-true.webp'
      - 'rs=w-573,h-200,cg-true.webp'
      - 'cr=w-356,h-200.webp'
      - 'cr=w-151,h-200.webp'
      - 'cr=w-410,h-200.webp'
      - 'rs=w-538,h-200,cg-true.webp'
      - 'cr=w-256,h-129.webp'
      - 'cr=w-405,h-200.webp'
      - 'cr=w-570,h-166.webp'
      - 'cr=w-267,h-200.webp'
      - 'rs=w-533,h-200,cg-true-1695212063.webp'
    type: logo_cloud
    enabled: true
    title: 'Our Esteemed Partners'
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '1.0'
sitemap_changefreq: weekly
override_twitter_settings: true
updated_by: 75f2bfa7-b632-4287-85bd-a90b151514e0
updated_at: 1695375947
meta_title: 'International Financial Literacy Conclave'
meta_description: "We are thrilled to unveil the upcoming 6th edition of the International Financial Literacy Conclave, proudly hosted by AWOKE India. Mark your calendars for November 24, 2023, as we gather once again to explore this year's compelling theme: Financial Inclusion: Bridging the Gap."
canonical_url: /
og_title: 'International Financial Literacy Conclave 2023'
og_description: "We are thrilled to unveil the upcoming 6th edition of the International Financial Literacy Conclave, proudly hosted by AWOKE India. Mark your calendars for November 24, 2023, as we gather once again to explore this year's compelling theme: Financial Inclusion: Bridging the Gap."
twitter_title: 'International Financial Literacy Conclave 2023'
twitter_description: "We are thrilled to unveil the upcoming 6th edition of the International Financial Literacy Conclave, proudly hosted by AWOKE India. Mark your calendars for November 24, 2023, as we gather once again to explore this year's compelling theme: Financial Inclusion: Bridging the Gap."
override_twitter_card_settings: false
---
