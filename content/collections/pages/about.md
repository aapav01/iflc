---
id: 12c20b72-3a80-4f92-ac5b-af014fc66a48
blueprint: page
title: About
author: 75f2bfa7-b632-4287-85bd-a90b151514e0
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
updated_by: 75f2bfa7-b632-4287-85bd-a90b151514e0
updated_at: 1695472111
custom_page_header: true
header_version: header_3
has_subtitle: true
subtitle: 'International Financial Literacy Conclave 2023'
---
## Concept Note 2023

India is on the cusp of a fast growth trajectory with a vision of achieving a $5 trillion economy by 2023–25. It is already the fifth-largest economy in the world and is soon to become the third, with a declared commitment to be one of the developed nations by 2047. According to Goldman Sachs, India's economy is on track to surpass the US by 2075 to become the world's second-largest after China. The World Bank projects India’s GDP surging to $52.5 trillion by then, compared with forecasts of $57 trillion and $51.5 trillion for China and the US, respectively. India's growth will be powered mainly by its large labour force, advances in technology, and rising capital investment, the Bank said in a recent report. These achievements are on the back of a series of transformative interventions made by India, like Make in India, Digital India, Startup India, massive investments in infrastructure, and many other initiatives. The cornerstone of India’s economic policy has been inclusive growth that reaches out to the poorest. India is committed to achieving the 17 SDGs set by the United Nations, which encompass a wide range of social, economic, and environmental objectives. Financial inclusion has been identified by the World Bank as an enabler for seven out of the 17 Sustainable Development Goals. 

Financial literacy and financial inclusion play crucial roles in India's economic growth targets. It equips individuals with the knowledge and skills to make informed decisions about managing their finances, investing, and planning. By extending financial services to the unbanked and underbanked populations, financial inclusion promotes economic participation and empowers individuals to utilise financial resources effectively. It promotes entrepreneurship by providing MSMEs with access to credit, financial products, and services so they can expand their operations, invest in technology, practice responsible borrowing and improve their financial stability. As more individuals and businesses gain access to formal financial services, they are more likely to participate in the formal economy, leading to increased tax revenues, better regulation, and improved economic governance.

Under the National Mission for Financial Inclusion, India's flagship program named Pradhan Mantri Jan Dhan Yojana, was launched in August 2014. It is a pioneering financial inclusion initiative, arguably the biggest and most successful in the world, that aims to provide universal access to banking services for all citizens, especially the marginalised and underprivileged. The program's uniqueness lies in its widespread reach, with over 500 million bank accounts opened since its inception. Jan Dhan stands as a testament to India's commitment to ensuring financial inclusion for every citizen.

The Reserve Bank of India developed a Financial Inclusion Index to measure the level of financial inclusion. In the year 2017, it was estimated at 43.4; it rose to 53.9 in 2021, then to 56.4 in 2022. Notwithstanding the astounding success of the Jandhan Yojana, the country still faces significant challenges in financial inclusion with a large unbanked population, especially in rural areas and among women. Limited technology infrastructure, low financial and digital literacy, and gender gaps further hinder inclusion efforts. The Jan Dhan Yojana has made progress in opening bank accounts, but access to a broader range of financial services, the reluctance of account holders to route their transactions through these accounts, on-boarding, retention, and sustained operations of accounts, avoiding timely repayments of loans, etc. still remain challenges. NBFCs, small finance banks, and FinTech organisations, through technology, do address the unbanked and underbanked segments, offering innovative products and services, but financial literacy, even among the educated, remains a work in progress even now.

## Objectives

The objective of the conclave is to bring together domain experts both from India and abroad and various stake holders to deliberate on:
* Various Financial Inclusion initiatives taken for MSMEs, Youth, Women and Farmers,
* Gaps and challenges in implementing Financial Inclusion measures and strategies to address them effectively and,
* Successful case studies and best practices in strengthening Financial Inclusion efforts. 

The deliberations will be through presentations and interactions by panels in five technical sessions.