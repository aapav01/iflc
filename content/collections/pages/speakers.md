---
id: dbe134cc-f9d1-49aa-ae57-4f2f43880a29
blueprint: page
title: Speakers
author: 75f2bfa7-b632-4287-85bd-a90b151514e0
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
updated_by: 75f2bfa7-b632-4287-85bd-a90b151514e0
updated_at: 1695272917
custom_page_header: true
blocks:
  -
    id: lmrzegrg
    version: team_section_4
    title: 'Galaxy of Dignitaries at #IFLC2022'
    description: 'The objective for the #IFLC22 pivots around this key issue of bringing digitalisation to serve the purpose of skilling & also safeguarding the citizens on financial matters at a speed equal to/greater than a storm of digital financial products & services.'
    members:
      -
        id: lmrmvekq
        full_name: 'Shri Brajesh Pathak'
        position: 'Deputy Chief Minister'
        biography: 'Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
        image: 'rs=w-500,h-500,cg-true.webp'
      -
        id: lmrngmqv
        full_name: 'Shri Manoj Kumar Singh'
        position: 'IAS, Agriculture Production'
        biography: 'Commissioner, Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
        image: 'rs=w-400,h-400,cg-true.webp'
      -
        id: lmrnju3q
        image: 'rs=w-600,h-600,cg-true.webp'
        full_name: 'Shri Navneet Sehgal'
        position: 'IAS, Additional Chief Secretary'
        biography: 'Sports & Youth Welfare, Uttar Pradesh'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnlwq8
        image: 'rs=w-730,h-730,cg-true.webp'
        full_name: 'Shri Ashwani Bhatia'
        position: 'Whole Time Member'
        biography: 'Securities and Exchange Board of India, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnndbl
        image: 'rs=w-730,h-730,cg-true-2.webp'
        full_name: 'G. P. Garg'
        position: 'Executive Director'
        biography: 'Securities and Exchange Board of India, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmrnq6x8
        image: 'rs=w-730,h-730,cg-true-3.webp'
        full_name: 'Prasun Kumar Das'
        position: 'Secretary General - Asia Pacific'
        biography: 'Rural and Agricultural Credit, Association, Bangkok, Thailand'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsnqhaw
        image: 'rs=w-730,h-730,cg-true-1695269599.webp'
        full_name: 'Anil Nair'
        position: 'Founder - ThinkStreet'
        biography: Mumbai
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsnwxrq
        image: 'rs=w-730,h-730,cg-true-2-1695269915.webp'
        full_name: 'Mrin Agarwal'
        position: 'Founder & Director - Finsafe'
        biography: 'Co founder - Womantra, Bengaluru'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsnxjkh
        image: 'rs=w-730,h-730,cg-true-3-1695269961.webp'
        full_name: 'S. K. Dora'
        position: 'Chief General Manager - NABARD'
        biography: 'Regional Office, Lucknow'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsnzxtg
        image: 'rs=w-730,h-730,cg-true-4.webp'
        full_name: 'Devesh Mathur'
        position: 'Chief Operating Officer'
        biography: 'Bank Of America, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmso0d81
        image: 'rs=w-730,h-730,cg-true-5.webp'
        full_name: 'Neeraj Kulshrestha'
        position: 'Chief Regulatory Officer'
        biography: 'BSE Ltd, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmso1x85
        image: 'rs=w-730,h-730,cg-true-6.webp'
        full_name: 'Amit Saxena'
        position: 'MD & CEO'
        biography: 'E-kosh, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmso54pe
        image: 'rs=w-730,h-730,cg-true-7.webp'
        full_name: 'Prateek Pant'
        position: 'Chief Business Officer'
        biography: 'White Oak Capital Management, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmso7h9n
        image: 'rs=w-730,h-730,cg-true-8.webp'
        full_name: 'Ramesh Mittal'
        position: Director
        biography: 'CCS National Institute of Agricultural Marketing,Rajasthan'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmspa5vo
        image: 'rs=w-730,h-730,cg-true-9.webp'
        full_name: 'Christopher Turillo'
        position: Co-Founder
        biography: 'Medha Foundation, Lucknow'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmspb8yr
        image: 'cr=w-730,h-730.webp'
        full_name: 'Annapurna Ayyappan'
        position: 'Assistant Programme Specialist'
        biography: 'UNESCO Institute for Lifelong Learning, Hamburg Germany'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsphrsb
        image: 'rs=w-400,h-400,cg-true-1695272646.webp'
        full_name: 'Manish Sinha'
        position: 'General Manager'
        biography: 'SIDBI, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmspkcu4
        image: 'rs=w-730,h-730,cg-true-1695272709.webp'
        full_name: 'Prachi Tewari Gandhi'
        position: 'Sociologist & Founder'
        biography: 'Aananda Foundation, Gurugram'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsplp5x
        image: 'rs=w-730,h-730,cg-true-2-1695272839.webp'
        full_name: 'Murali Krishna'
        position: Director
        biography: 'Plaudit Services Private Limited, Hydrabad'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsponp9
        image: 'rs=w-400,h-400,cg-true-2.webp'
        full_name: 'Kapil Dev'
        position: 'Chief Business Officer'
        biography: 'NCDEX, Mumbai'
        toggle_social: false
        type: set
        enabled: true
      -
        id: lmsppkwu
        full_name: 'Brajesh Singh'
        position: 'General Manager'
        biography: 'Bank of Baroda'
        toggle_social: false
        type: set
        enabled: true
        image: 'rs=w-730,h-730,cg-true,m.webp'
    type: team_section
    enabled: true
header_version: header_3
has_subtitle: false
---
