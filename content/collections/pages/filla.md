---
id: af7ec424-57df-4135-9acc-52d16b834bec
blueprint: page
title: FiLLA
custom_page_header: false
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
updated_by: 75f2bfa7-b632-4287-85bd-a90b151514e0
updated_at: 1695457606
blocks:
  -
    id: lmt051zm
    version: slider_2
    slides:
      -
        id: lmt06ig2
        type: new_set
        enabled: true
        title: 'Financial Inclusion and Literacy Leadership Award'
        overlay_text: 'FiLLA 2023'
        image: sliders/dsc_0622.png
    type: slider
    enabled: true
  -
    id: lmt0hwls
    version: counter_2
    title: 'Last Date to Submit Nomination Form'
    date_field: '2023-10-15'
    type: countdown
    enabled: true
  -
    id: lmt0xf0w
    version: feature_multiple_2
    title: 'Award Categories'
    features:
      -
        id: lmt0xtd4
        title: "FiLL Award Institutional\0"
        has_link: false
        type: card
        enabled: true
        image: bank.svg
      -
        id: lmt0yaaw
        title: "FiLL Award Individual\0"
        has_link: false
        type: card
        enabled: true
        image: man.svg
      -
        id: lmt0yt30
        title: "FiLL Award Innovation\0"
        has_link: false
        type: card
        enabled: true
        image: light-bulb.svg
    type: feature_multiple
    enabled: true
  -
    id: lmt1dy6y
    version: contact_3
    enable_location: false
    enable_form: true
    form_title: 'Nomination Form'
    type: contact_section
    enabled: true
    contact_form: nomination_form
---
# Award Selection Process & Rules

**Key parameters for evaluation by the jury:**
1.  The numbers (qualitative details as asked in the form)
2.  Timelines (Specific Dates & Period wherever asked in the form should be necessarily filled)
3.  Evidence based responses: The applicants should provide details of recognition, rewards etc	including picture, video links, websites etc with full details.

**Besides the above, the following parameters play an important role in final decision:**
1.  The numbers, timelines & evidence based responses are very important.
2.  Tangible impact on society
3.  Scale of efforts in reach and numbers
4.  Segments covered
5.  Books / Articles / Journals Published
6.  Use of Technology
7.  Sustainability
8.  Any previous recognition/Award
9.  On-field contribution to bridge economic inequality

**Adherence to values, ethics, integrity and contribution to societal development would be a common consideration for all awards**

AWOKE India Foundation (AIF) founded in 2012, is a civil society organization (registered under Sec 8 of the Companies Act) dedicated to financial literacy, awareness, inclusion. AIF is the only SEBI recognised Investor Association (IA). These awards are being conferred since 2018.  To learn more, please visit our websites: www.awokeindia.com, www.awokeindiafoundation.com, www.iflc.in