<?php

namespace App\Gateways;

use DoubleThreeDigital\SimpleCommerce\Contracts\Gateway;
use DoubleThreeDigital\SimpleCommerce\Contracts\Order;
use DoubleThreeDigital\SimpleCommerce\Gateways\BaseGateway;
use Illuminate\Http\Request;
use Razorpay\Api\Api;

class Razorpay extends BaseGateway implements Gateway
{
    public function name(): string
    {
        return __('Razorpay');
    }

    public function prepare(Request $request, Order $order): array
    {
        return [];
    }

    public function checkout(Request $request, Order $order): array
    {
        $this->markOrderAsPaid($order);

        return [];
    }

    public function checkoutRules(): array
    {
        return [];
    }

    public function checkoutMessages(): array
    {
        return [];
    }

    public function refund(Order $order): array
    {
        return [];
    }

    public function webhook(Request $request)
    {
        //
    }

    public function fieldtypeDisplay($value): array
    {
        return [
            'text' => $value['data']['id'],
            'url' => null,
        ];
    }
}
